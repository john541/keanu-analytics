Keanu Analytics
===============

This is a reimplementation of the `prototype
<https://gitlab.com/keanuapp/keanu-analytics-prototype/>`_ collector that was
written in Clojure and is is retired.

Features
--------

☑ Real-Time Mode: run as a Synapse Application Service to collect and tally
metrics in real time

☑ Database Persistence: store events into a PostgreSQL database

☑ Backfill API: provide a means to add past events given only a sender, event
type and timestamp (with "best effort" data resolved from Synapse)

☐ Reporting: generate CSV files on demand for a list of periods

☐ Publish: publish the reported stats to a google drive sheet or somehow make
it accesible to analysts

☑ Configurable: all inputs should be configurable at run-time with configs

Definitions
-----------

- user: a matrix user identified by a unique MXID (e.g., @foobar:neo.keanu.im)
- group: a matrix room containing three or more users
- group message: a message sent in a group
- private message: a message sent in a room containing exactly two users
- engaged: sent at least one text message
- user's client: the client name reported by the most recently used device
- user's ip: the last seen ip address of the most recently used device

Storage Schema
--------------

The prototype used this schema to store events:

.. code-block:: sql

   CREATE TABLE atomic.events (
     period                     TEXT,
     event                      TEXT,
     bucket                     TEXT,
     appname                    TEXT,
     platform                   TEXT,
     is_user_engagement_period  BOOLEAN,
     is_user_engagement_first   BOOLEAN,
     is_federated               BOOLEAN,
     is_group                   BOOLEAN,
     is_group_engagement_period BOOLEAN
   );

This doesn't include any necessary caches.

If the new metrics collector uses this schema, then the prototype's original
sql queries can be used with minimal modification:

https://gitlab.com/keanuapp/keanu-analytics-prototype/-/blob/master/resources/sql/queries.sql

Calendar periods
----------------

This implementation uses ISO 8601 weeks, while the prototype used the `4 4 5
fiscal year calendar <http://en.wikipedia.org/wiki/4-4-5_calendar>`_.

The only difference between these systems is that the year of the week now
belongs to the year in which the Thursday falls, instead of the year ending on
the week containing the last weekday of the year.

### Appname buckets

These should be configurable at runtime (list of regex mappings in a json/yaml).

Test inputs are available in the prototype: 

https://gitlab.com/keanuapp/keanu-analytics-prototype/-/blob/master/test/clj/keanu_analytics/test/routes/services.clj#L31-83

# Reporting

## Columns

### REPORT: user engagement, segmented

- period: the week being reported on
- date: the first day in the period
- appname: the normalized name of the user's client
- geo (bucket): the geo region that the user's most recently seen ip address was geolocated into. (one of tibet, china, other)
- users engaged (first ever): the number of users who were engaged for the first time ever this period
- users engaged (per period): the number of users who engaged this period

### REPORT: aggregate engagement

- period: the week being reported on
- date: the first day in the period
- groups engaged: the unique number of groups engaged in the period
- private messages sent: the total number of private messages sent in the period
- group messages sent: the total number of messages sent in groups in the period

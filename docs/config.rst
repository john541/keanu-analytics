Configuration
=============

All configuration for Keanu Analytics is contained with a single directory.
On Linux and BSD systems, this should either be
`$XDG_CONFIG_HOME/KeanuAnalytics` or `/etc/KeanuAnalytics`, and will be searched
in that order.

The core configuration is held in `config.yaml`. An example file would look
like:

.. code-block:: yaml

    ---
    synapse:
      homeserver_fqdn: synapse.example.com
      as_token: "eeTiidoo7ipei2aiCeif9chu"
      hs_token: "epaiDoh9Ohd8eecie8ief9nu"
    database:
      name: analytics
      user: analytics
      password: secret123
    geo_regions:
      database: GeoLite2-City.mmdb
      buckets:
        - name: ma
          path: ma.geojson
        - name: usa
          path: usa.geojson
    user_agents:
        keanu: '^keanu_example/.*'
        riot-desktop: '^mozilla/5\.0.+riot/[0-9].+electron/[0-9].*$'
        riot.im: '^riot\.im/.*$'
        riot: '^riot/.*'

Filename paths in the configuration file are relative to the configuration
directory.

Synapse Homeserver
------------------

Keanu Analytics receives events via the Application Service API and queries
Synapse via the Client Server API. The following details are required for
operation:

+-------------------+-----------------------------------------------------+
| homeserver_fqdn   | The fully qualified domain name for the Synapse     |
|                   | server. This is used for connecting back to Synapse |
|                   | using the Client Server API and also to determine   |
|                   | that users in events are local to the homeserver.   |
+-------------------+-----------------------------------------------------+
| as_token          | This is the token used by the Application Service   |
|                   | (Keanu Analytics) to authenticate to the homeserver |
|                   | to use the Client Server API. This is a shared      |
|                   | secret.                                             |
+-------------------+-----------------------------------------------------+
| hs_token          | This is the token used by the Homeserver to         |
|                   | authenticate to the Application Service using the   |
|                   | Application Service API. This is a shared secret.   |
+-------------------+-----------------------------------------------------+

Database
--------

A `PostgreSQL <https://www.postgresql.org/>`_ database is required to store
events for later reporting, and to keep records to avoid double-counting events.
The following details are required for operation:

+-------------------+-----------------------------------------------------+
| name              | PostgreSQL database name                            |
+-------------------+-----------------------------------------------------+
| user              | PostgreSQL user name                                |
+-------------------+-----------------------------------------------------+
| password          | PostgreSQL password                                 |
+-------------------+-----------------------------------------------------+

The database must be initialised before use by importing the schema on the
command line:

.. code-block::

   psql -U $user $name < kealytics/dbinit.sql
   Password for user $user: $pass

Geographic Regions
------------------

There are two components to geographic buckets in Keanu Analytics. The first
is a GeoIP database used to resolve IP addresses to a latitude and longitude.
The second is a series of GeoJSON geometries that are used to place those
co-ordinates into a bucket. City/region/country data from the GeoIP database is
not used.

The supported databases are:

* `Maxmind GeoLite2-City <https://dev.maxmind.com/geoip/geoip2/geolite2/>`_ (Free)
* `Maxmind GeoIP2-City <https://www.maxmind.com/en/geoip2-city>`_ (Paid)

The MaxMind terms of use may prohibit distribution and so no database is
bundled with the Keanu Analytics sources or builds.

The buckets are defined as a list, and the order of the list is preserved.
When placing users into buckets the first matching bucket will be used.
See :class:`GeoBuckets <kealytics.geo.GeoBuckets>` for more information.

User Agents
-----------

While platforms (operating systems) are matched automatically using an external
library, application names are matched based on administrator provided regular
expressions.

.. warning::

   User agents are always normalised to lower case before matching against the
   regular expression, so be sure to only use lower case characters within the
   regular expression or it will never match!

.. warning::

   Backslashes are common in regular expressions, but are also used in YAML and
   Python for escape sequences. Ensure that regular expressions using
   backslashes are enclosed in 'single quotes' and not "double quotes". Inside
   single quotes backslashes do not require escaping.

The configuration of user agents consists of a series of key-value pairs with
the name of the app and the regular expression to match with. If no regular
expression matches the app will be recorded as "other".

Google Drive
------------

An OAuth2 client must be registered with Google to allow use of the Google
Drive API, and a folder name must be specified for uploads. The following
details are required for operation:

+-------------------+-----------------------------------------------------+
| client_id         | OAuth2 Client Identifier                            |
+-------------------+-----------------------------------------------------+
| client_secret     | OAuth2 Client Secret                                |
+-------------------+-----------------------------------------------------+
| folder_name       | Destination Folder Name                             |
+-------------------+-----------------------------------------------------+

To register an OAuth2 client you must:

* Visit the Google Developers Console and `Create a Project
  <https://console.developers.google.com/projectcreate>`_
* `Enable the Drive API
  <https://developers.google.com/drive/api/v3/enable-drive-api>`_
* `Set a Project Name
  <https://console.developers.google.com/apis/credentials/consent>`_ (but other
  fields can be left as they are)
* `Create an OAuth 2.0 Client ID
  <https://console.developers.google.com/apis/credentials/oauthclient>`_
* Select 'Desktop app' when creating the ID

The client ID and client secret should be placed into the `client_id` and
`client_secret` fields of the configuration file respectively.

The folder name does not have to exist yet.

.. warning::

   If two folders exist with the same name (this is, for some reason, allowed
   even under the same folder...) then unexplained things will happen. It is
   best to avoid this situation by not creating more than one folder with the
   same name anywhere in your Google Drive.

It is also necessary to login with the Google Account you would like to use to
upload to Google Drive. This step is described in :ref:`Logging in to Google
Drive <gdrive_login>` and can be performed later.

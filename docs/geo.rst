Geographic Regions
==================

Users are sorted into buckets for geographic regions based on matching their
latitude and longitude to a geometry provided in GeoJSON format.

.. automodule:: kealytics.geo
   :members:
   :undoc-members:

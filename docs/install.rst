Installation
============

Development Setup
-----------------

Keanu Analytics requires Python 3.8 (but should be working on Debian stable
later).

System Dependencies
~~~~~~~~~~~~~~~~~~~

The easiest way to setup a development instance is to use virtualenv. You will
also need some development libraries to build some of the dependencies.

On Debian systems, you can install this with:

.. code-block:: bash

   sudo apt install build-essential python3-dev python3-venv

Clone Sources
~~~~~~~~~~~~~

Development should happen against the master branch of the Keanu Analytics
repository `hosted at GitLab <https://gitlab.com/keanuapp/keanu-analytics>`_.

.. code-block:: bash

   git clone https://gitlab.com/keanuapp/keanu-analytics
   cd keanu-analytics

Virtual Environment
~~~~~~~~~~~~~~~~~~~

Create and activate the virtual environment with:

.. code-block:: bash

   python3 -m venv env
   . env/bin/activate

Your prompt should be prefixed with "(env)" if this is successful. This may be
hidden if you use excessive plugins in your shell.

Local Dependencies
~~~~~~~~~~~~~~~~~~

Local dependencies can now be installed with pip:

.. code-block:: bash

   pip install wheel
   pip install -r requirements.txt

These dependencies will be installed into the virtual environment and will not
affect the rest of the system. System packages will be ignored by Python running
inside the virtual environment.

Adding the Application Service to Synapse
-----------------------------------------

In your Synapse configuration directory, add a new file named
`app_keanu_analytics.yaml`.

.. literalinclude:: ../examples/app_keanu_analytics.yaml
   :language: yaml

You should replace the `as_token` and `hs_token`. On Debian systems, you can
create two random strings for this purpose with:

.. code-block:: bash

   sudo apt install pwgen
   pwgen 30 2

Next Step: Configuration
------------------------

You can now proceed to the :doc:`configuration instructions <config>`.

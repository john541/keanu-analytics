Reports
=======

Reports are generated with the `report.py` script, which is itself a wrapper
to write out a couple of CSV files from the database using an SQL query.

Reporting Periods
-----------------

The aggregation period for all reports is one week, starting on a Monday, using
`ISO 8601 week dates <https://en.wikipedia.org/wiki/ISO_week_date>`_.

.. warning::
   The ISO year is slightly offset to the Gregorian year; for example, Monday
   30 December 2019 in the Gregorian calendar is the first day of week 1 of
   2020 in the ISO calendar.

Aggregate Report
----------------

The "Aggregate Report" contains the total number of groups engaged, and the
total number of group and private messages sent during each period.

+------------------------+-------------------------------------------------+
| Column                 | Description                                     |
+========================+=================================================+
| period                 | the reporting period as an ISO 8601 week date   |
+------------------------+-------------------------------------------------+
| date                   | the first day of the reporting period           |
|                        | (YYYY-MM-DD)                                    |
+------------------------+-------------------------------------------------+
| groups engaged         | the unique number of groups engaged (at least   |
|                        | one message sent) in the period                 |
+------------------------+-------------------------------------------------+
| private messages sent  | the total number of messages sent in rooms with |
|                        | exactly two members during the period           |
+------------------------+-------------------------------------------------+
| group messages sent    | the total number of messages sent in rooms with |
|                        | more that two members during the period         |
+------------------------+-------------------------------------------------+

Segmented Report
----------------

The "Segmented Report" contains user counts segmented by application name,
platform and geolocation bucket. This report contains a flat data table but
would lend itself to quick analysis using pivot tables.

+------------------------+-------------------------------------------------+
| Column                 | Description                                     |
+========================+=================================================+
| period                 | the reporting period as an ISO 8601 week date   |
+------------------------+-------------------------------------------------+
| date                   | the first day of the reporting period           |
|                        | (YYYY-MM-DD)                                    |
+------------------------+-------------------------------------------------+
| appname                | the normalized name of the user's client        |
+------------------------+-------------------------------------------------+
| platform               | the normalized name of the user's platform      |
+------------------------+-------------------------------------------------+
| bucket                 | the geo region that the user's most recently    |
|                        | seen ip address was geolocated into             |
+------------------------+-------------------------------------------------+
| engaged_users_new      | the number of users who engaged this period     |
+------------------------+-------------------------------------------------+
| engaged_users_period   | the number of users who were engaged for the    |
|                        | first time ever this period                     |
+------------------------+-------------------------------------------------+


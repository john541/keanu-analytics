-- period: the week being reported on
-- date: the first day in the period
-- groups engaged: the unique number of groups engaged in the period
-- private messages sent: the total number of private messages sent in the period
-- group messages sent: the total number of messages sent in groups in the period

SELECT
e1.period,
TO_CHAR(TO_DATE(e1.period, 'IYYY"-W"IW'), 'YYYY-MM-DD') AS date,
(SELECT COUNT(*) FROM analytics.events AS e2 WHERE
  e1.period = e2.period AND
  e2.event_type = 3
 GROUP BY e2.period) AS groups_engaged,
(SELECT COUNT(*) FROM analytics.events AS e2 WHERE
  e1.period = e2.period AND
  e2.event_type = 2
 GROUP BY e2.period) AS private_messages,
(SELECT COUNT(*) FROM analytics.events AS e2 WHERE
  e1.period = e2.period AND
  e2.event_type = 1
 GROUP BY e2.period) AS group_messages
FROM analytics.events AS e1
GROUP BY e1.period

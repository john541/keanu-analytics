import copy
import os
import pickle
from typing import Dict
from typing import List
from typing import Optional
from typing import Union

import confuse

import google.oauth2.credentials
from google.auth.transport.requests import Request

from google_auth_oauthlib.flow import InstalledAppFlow

from googleapiclient.discovery import Resource
from googleapiclient.discovery import build
from googleapiclient.http import MediaFileUpload

SCOPES = ['https://www.googleapis.com/auth/drive']
"""Permission scopes required for OAuth client to request.

See https://developers.google.com/drive/api/v2/about-auth.
"""

REDIRECT_URI = "urn:ietf:wg:oauth:2.0:oob"
"""Redirect URI for the authorization code after completing login."""


BASE_CLIENT_CONFIG: Dict[str, Dict[str, Union[List[str], str]]] = {
    "installed": {
        "redirect_uris": [REDIRECT_URI],
        "auth_uri": "https://accounts.google.com/o/oauth2/auth",
        "token_uri": "https://accounts.google.com/o/oauth2/token"
    }
}

FileMetadata = Dict[str, str]


def create_file_metadata(filename: str, mimetype: str) -> FileMetadata:
    """Return file metadata for a Google Drive upload."""
    file_metadata = {
        'name': filename,
        'mimeType': mimetype,
    }
    return file_metadata


class GoogleDriveUploader:  # pragma: no cover
    _client_id: str
    _client_secret: str
    _creds: google.oauth2.credentials.Credentials
    _creds_dir: str
    _folder_name: str
    _service: Resource

    def __init__(self, client_id: str, client_secret: str, creds_dir: str,
                 folder_name: str):
        self._creds_dir = creds_dir
        self._client_id = client_id
        self._client_secret = client_secret
        self._folder_name = folder_name

    def setup(self) -> None:
        client_config = copy.deepcopy(BASE_CLIENT_CONFIG)
        client_config['installed']['client_id'] = self._client_id
        client_config['installed']['client_secret'] = self._client_secret
        flow = InstalledAppFlow.from_client_config(client_config,
                                                   SCOPES,
                                                   redirect_uri=REDIRECT_URI)
        auth_uri = flow.authorization_url()
        print("Visit the following URL to obtain your authorization code:")
        print(auth_uri)
        code = input('Enter the authorization code: ').strip()
        flow.fetch_token(code=code)  # TODO: returns?
        self._creds = flow.credentials
        self._service = build('drive', 'v3', credentials=self._creds)
        with open(os.path.join(self._creds_dir, 'gdrive.pickle'),
                  'wb') as token:
            pickle.dump(self._creds, token)

    def restore(self) -> None:
        pickle_path = os.path.join(self._creds_dir, 'gdrive.pickle')
        if os.path.exists(pickle_path):
            with open(os.path.join(pickle_path), 'rb') as token:
                self._creds = pickle.load(token)
            # TODO: If there are no (valid) credentials available, let the user
            # log in.
            if not self._creds or not self._creds.valid:
                if self._creds and self._creds.expired:
                    if self._creds.refresh_token:
                        self._creds.refresh(Request())
            self._service = build('drive', 'v3', credentials=self._creds)

    def upload_file(self, file_path: str) -> str:
        """Upload a file from the local filesystem or return an existing ID.

        The basename of the path will be used as the destination filename. The
        destination file must not exist in upload folder. The folder will
        be created if it does not exist.

        :param file_path: local filesystem path to the file to upload
        :returns: file ID
        :raises RuntimeError: file already exists in the given folder
        """
        file_name = os.path.basename(file_path)
        folder_id = self.ensure_folder()
        file_id = self.identify_file(file_name, 'text/csv', folder_id)
        if file_id is None:
            file_metadata = {
                'name': file_name,
                'parents': [folder_id],
            }
            media = MediaFileUpload(file_path,
                                    mimetype='text/csv')
            uploaded_file = self._service.files().create(body=file_metadata,
                                                         media_body=media,
                                                         fields='id').execute()
            return uploaded_file.get('id')
        raise RuntimeError(f"File named {file_name} already exists in "
                           f"{folder_id}: {file_id}")

    def ensure_folder(self) -> str:
        """Ensure the upload folder exists and return its ID.

        .. warning::

           This function does not care where the folder is, just cares about
           the name. Bad Things™ will happen if you have more than one folder
           with the same name.

        :returns: folder ID
        """
        folder_id = self.identify_file(
            self._folder_name, "application/vnd.google-apps.folder")
        if not folder_id:
            folder_metadata = create_file_metadata(
                self._folder_name, "application/vnd.google-apps.folder")
            folder_object = self._service.files().create(body=folder_metadata,
                                                         fields='id').execute()
            return folder_object.get('id')
        return folder_id

    def identify_file(self, file_name: str, file_type: str,
                      folder_id: Optional[str] = None) -> Optional[str]:
        """Return the ID for a file matching given properties.

        :param file_name: base name of the file
        :param file_type: MIME type of the file
        :param folder_id: optional ID of the parent folder
        :returns file ID
        """
        q = f'"name = "{file_name}"'
        q += f' and mimeType = "{file_type}"'
        q += ' and trashed = false'
        if folder_id:
            q += f' and "{folder_id}" in parents'
        results = self._service.files().list(
            pageSize=10, q=q,
            fields="nextPageToken, files(id)").execute()
        items = results.get('files', [])
        for item in items:
            return item["id"]
        return None

    @classmethod
    def from_confuse(cls, sv: confuse.Subview):
        """Create a new GoogleDriveUploader from confuse configuration.

        The subview must contain an OAuth2 client ID and client secret for
        a client with the Google Drive API enabled.

        .. note::

           The configuration directory used by Confuse will also be chosen as
           the storage location for the user OAuth2 credentials.

        :param sv: a Confuse SubView with the required keys
        :returns: the new GoogleDriveUploader
        """
        client_id = sv['client_id'].get(str)
        client_secret = sv['client_secret'].get(str)
        folder_name = sv['folder_name'].get(str)
        creds_dir = sv.root().config_dir()
        return cls(client_id, client_secret, creds_dir, folder_name)

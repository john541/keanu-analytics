from typing import Dict

import confuse

from pkg_resources import resource_stream

import psycopg2


class KeanuReports:  # pragma: no cover
    def __init__(self, config: confuse.Configuration):
        dbargs: Dict[str, str] = {}
        for dbkey in ["dbname", "user", "password", "host", "port"]:
            v: str = config["database"][dbkey].get(str)
            if v != "default_sentinel":
                dbargs[dbkey] = v
        self._conn = psycopg2.connect(**dbargs)

    def init_db(self) -> None:
        query = resource_stream("kealytics",
                                "dbinit.sql").read().decode("utf-8")
        with self._conn:
            with self._conn.cursor() as cur:
                cur.execute(query)

    def aggregate_report(self, file_name: str) -> None:
        query = resource_stream("kealytics",
                                "aggregate_report.sql").read().decode("utf-8")
        copy_query = f"COPY ({query}) TO STDOUT WITH CSV HEADER"
        with open(file_name, 'w') as report_output:
            with self._conn:
                with self._conn.cursor() as cur:
                    cur.copy_expert(copy_query, report_output)

    def segmented_report(self, file_name: str) -> None:
        query = resource_stream("kealytics",
                                "segmented_report.sql").read().decode("utf-8")
        copy_query = f"COPY ({query}) TO STDOUT WITH CSV HEADER"
        with open(file_name, 'w') as report_output:
            with self._conn:
                with self._conn.cursor() as cur:
                    cur.copy_expert(copy_query, report_output)


if __name__ == "__main__":
    config = confuse.Configuration('KeanuAnalytics', 'kealytics')
    r = KeanuReports(config)
    r.aggregate_report("/tmp/aggregate_report.csv")
    r.segmented_report("/tmp/segmented_report.csv")

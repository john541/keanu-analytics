from datetime import datetime

from nose.tools import assert_equal

from kealytics.period import period_for_timestamp

def check_period(ts, expected):
    assert_equal(period_for_timestamp(ts), expected)

def test_period():
    tests = [
        (datetime(2019, 1, 1).timestamp() * 1000, "2019-W1"),
        (datetime(2019, 11, 11).timestamp() * 1000, "2019-W46"),
        (datetime(2019, 12, 30).timestamp() * 1000, "2020-W1"),
        (1567787819000, "2019-W36"),
    ]
    for test in tests:
        yield (check_period, *test)

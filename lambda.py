import sys
import traceback

from derezzed.geo import GeoRegionChain
from derezzed.ip import GeoIPCity

from kealytics.lambda_util import db_connect
from kealytics.lambda_util import geoip_db
from kealytics.lambda_util import region_config


conn = db_connect()
city = GeoIPCity(geoip_db())
region = GeoRegionChain(region_config())

def lambda_handler(event, context):
    try:
        print(conn)
        print(city)
        print(region)
    except:
        traceback.print_exc(file=sys.stdout)